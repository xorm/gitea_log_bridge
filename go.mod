module gitea.com/xorm/gitea_log_bridge

go 1.12

require (
	code.gitea.io/log v0.0.0-20190526010349-0560851a166a
	xorm.io/core v0.7.0
)
